#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import Eval, Bool
from trytond.transaction import Transaction
from trytond.pool import Pool


class BillingLine(ModelSQL, ModelView):
    _name = 'account.invoice.billing_line'

    payment_amount = fields.Numeric('Amount Payment',
            digits=(16, Eval('currency_digits', 2)), states={
                'required': Bool(Eval('payment_date')),
                'readonly': Bool(Eval('invoice_line')),
            }, depends=['currency_digits', 'payment_date', 'invoice_line'],
            on_change=['payment_amount'])
    payment_date = fields.Date('Date Payment', states={
                'required': Bool(Eval('payment_amount')),
                'readonly': Bool(Eval('invoice_line')),
            }, depends=['payment_amount', 'invoice_line'])
    payment_currency = fields.Many2One('currency.currency','Currency Payment',
            states={
                'required': Bool(Eval('payment_amount')),
                'readonly': Bool(Eval('invoice_line')),
            }, depends=['payment_amount', 'invoice_line'])
    currency_digits = fields.Function(fields.Integer('Currency Digits',
            on_change_with=['payment_currency']), 'get_currency_digits')

    payment_journal = fields.Many2One('account.journal', 'Journal',
            domain=[('type','=','cash')], states={
                'required': Bool(Eval('payment_date')),
                'readonly': Bool(Eval('invoice_line')),
            }, depends=['payment_date', 'invoice_line'])

    def default_currency_digits(self):
        return 2

    def get_currency_digits(self, ids, name):
        res = {}
        for line in self.browse(ids):
            if line.payment_currency:
                res[line.id] = line.payment_currency.digits
            else:
                res[line.id] = 2
        return res

    def on_change_with_currency_digits(self, vals):
        currency_obj = Pool().get('currency.currency')
        if vals.get('currency'):
            currency = currency_obj.browse(vals['currency'])
            return currency.digits
        return 2

    def on_change_payment_amount(self, vals):
        company_obj = Pool().get('company.company')
        currency_obj = Pool().get('currency.currency')
        res = {}
        if Transaction().context.get('company'):
            company = company_obj.browse(Transaction().context['company'])
            res['payment_currency'] = company.currency.id
        return res

    def _create_invoice_lines(self, lines, invoice_id, accounting_date=None):
        res = super(BillingLine, self)._create_invoice_lines(
            lines, invoice_id, accounting_date=None)
        self._create_payment_lines(lines, invoice_id)
        return res

    def _create_payment_lines(self, lines, invoice_id):
        payment_line_obj = Pool().get('account.invoice.payment_line')

        if not isinstance(lines, (list, tuple)):
            lines = [lines]
        for line in lines:
            if line.payment_date:
                vals = {
                    'amount': line.payment_amount,
                    'date': line.payment_date,
                    'currency': line.payment_currency.id,
                    'invoice': invoice_id,
                    'journal': line.payment_journal.id,
                    'description': line.description,
                    }
                payment_line_id = payment_line_obj.create(vals)

BillingLine()
