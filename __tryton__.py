#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Invoice Billing Cash',
    'name_de_DE': 'Fakturierung Rechnungsstellung aus Abrechnungsdaten Barzahlung',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''Account Invoice Billing Cash
    - Adds the possibility to register cash payments to invoices created directly
      from billing lines listed in a separate table.
''',
    'description_de_DE': '''Fakturierung Rechnungsstellung aus Abrechnungsdaten mit Barzahlung
    - Erweitert die Generierung von Rechnungen auf Basis von in einer
      Tabelle erfassten Abrechnungsdaten um die Möglichkeit der direkten
      Erfassung von Barzahlungseingängen.
''',
    'depends': [
        'account_invoice_billing',
        'account_invoice_payment_batch_capture',
    ],
    'xml': [
        'billing.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
